import React from 'react';

export type TabsContextProps = {
  hide: boolean;
  setHide: React.Dispatch<React.SetStateAction<boolean>>;
};

const TabsContext = React.createContext<TabsContextProps>({
  hide: false,
  setHide() {},
});

export function TabsProvider(props: React.PropsWithChildren) {
  const { children } = props;

  const [hide, setHide] = React.useState<boolean>(false);

  const value = {
    hide,
    setHide,
  };

  return (
    <TabsContext.Provider value={value}>
      {children}
    </TabsContext.Provider>
  );
}

export default TabsContext;
