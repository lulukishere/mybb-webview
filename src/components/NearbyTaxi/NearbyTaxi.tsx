import React from 'react';
import { MapContainer, TileLayer, Marker } from 'react-leaflet';
import L from 'leaflet';
import 'leaflet/dist/leaflet.css';
import styles from './styles.module.css';

const CURRENT_LOCATION: L.LatLngExpression = [-6.245960495358794, 106.82571964081777]; // Dummy location
const ZOOM = 18;

function NearbyTaxi() {
  const mapRef = React.useRef<L.Map>(null);
  const [userPosition, setUserPosition] = React.useState<L.LatLngExpression>(
    CURRENT_LOCATION,
  );

  const setMapView = React.useCallback(
    (latLng: L.LatLngExpression, zoom: number) => {
      const targetPoint = mapRef.current
        ?.project(latLng, zoom)
        .subtract([0, -(window.innerHeight / 6)]);
      const targetLatLng = mapRef.current?.unproject(targetPoint as L.PointExpression, zoom);
      mapRef.current?.setView(targetLatLng as L.LatLngExpression, zoom);
      mapRef.current?.fitBounds(mapRef.current.getBounds(), { padding: [-500, 500] })
    },
    [],
  );

  React.useLayoutEffect(() => {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        setMapView([position.coords.latitude, position.coords.longitude], ZOOM);
        setUserPosition([position.coords.latitude, position.coords.longitude]);
      },
      (err) => {
        console.log(err);
        // Mock
        setMapView(CURRENT_LOCATION, 18);
        setUserPosition(CURRENT_LOCATION);
      },
      { enableHighAccuracy: true },
    );
  
    navigator.geolocation.watchPosition(
      (position) => {
        setUserPosition([position.coords.latitude, position.coords.longitude]);
      },
      (err) => {
        console.log(err);
      },
      { enableHighAccuracy: true },
    );

  }, [setMapView]);

  return (
    <MapContainer
      ref={mapRef}
      className={styles.map}
      zoomControl={false}
    >
      <Marker
        icon={L.divIcon({
          html: `<div class="${styles.position}"></div>`,
          iconSize: [20, 20],
          className: styles.markerIcon,
        })}
        position={userPosition}
      />
      <TileLayer
          attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
          url="https://tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
    </MapContainer>
  );
}

export default NearbyTaxi;
