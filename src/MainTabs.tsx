import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import {
  IonRouterOutlet,
  IonTabs,
  IonTabBar,
  IonTabButton,
  IonIcon,
  IonLabel,
} from '@ionic/react';
import { home, list } from 'ionicons/icons';
import useTabs from './hooks/useTabs';
import HomePage from './pages/HomePage';
import OrderWithNearbyPage from './pages/OrderWithNearbyPage';
import HistoryPage from './pages/HistoryPage';
import HistoryDetailPage from './pages/HistoryDetailPage';

function MainTabs() {
  const tabs = useTabs();
  console.log(tabs.hide);
  const tabBarStyles = React.useMemo(() => {
    if (!tabs.hide) {
      return;
    }

    return { display: 'none' };
  }, [tabs.hide]);

  
  return (
    <IonTabs>
      <IonRouterOutlet>
        <Redirect exact path="/" to="/home" />
        <Route path="/home" component={HomePage} />
        <Route path="/order" component={OrderWithNearbyPage} />
        <Route path="/history" component={HistoryPage} />
        <Route path="/history-detail/:id" component={HistoryDetailPage} />
      </IonRouterOutlet>
      <IonTabBar slot="bottom" style={tabBarStyles}>
        <IonTabButton tab="home" href="/home">
          <IonIcon icon={home} />
          <IonLabel>Home</IonLabel>
        </IonTabButton>
        <IonTabButton tab="order" href="/history">
          <IonIcon icon={list} />
          <IonLabel>Order</IonLabel>
        </IonTabButton>
      </IonTabBar>
    </IonTabs>
  );
}

export default MainTabs;
