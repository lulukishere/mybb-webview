import React from 'react';
import TabsContext from '../context/TabsContext';

export default function useTabs() {
  return React.useContext(TabsContext);
}
