import React from 'react';
import {
  IonPage,
  IonContent,
  IonModal,
  IonSearchbar,
  IonList,
  IonItem,
  IonIcon,
  IonLabel,
  useIonViewWillLeave,
} from '@ionic/react';
import { locationOutline, checkmarkCircle } from 'ionicons/icons';
import NearbyTaxi from '../../components/NearbyTaxi';
import './styles.css';

function OrderWithNearby() {
  const modalRef = React.useRef<HTMLIonModalElement>(null);

  useIonViewWillLeave(() => {
    modalRef.current?.dismiss();
  });

  return (
    <IonPage>
      <IonContent>
        <NearbyTaxi />
        <IonModal
          ref={modalRef}
          trigger="open-modal"
          isOpen={true}
          initialBreakpoint={0.5}
          breakpoints={[0.5, 1]}
          backdropDismiss={false}
          backdropBreakpoint={0.5}
        >
          <IonContent className="ion-padding">
            <IonSearchbar
              searchIcon={locationOutline}
              className="custom"
              onClick={() => modalRef.current?.setCurrentBreakpoint(1)}
              placeholder="Mau kemana?"
            />
            <IonList>
              <IonItem>
                <IonIcon icon={checkmarkCircle} slot="start" />
                <IonLabel>
                  <h2>Bluebird Office Park</h2>
                  <p>
                    Jl. Mampang Prapatan VII No.14, RT.9/RW.6, Tegal Parang, Kec. Mampang Prpt., Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12790
                  </p>
                </IonLabel>
              </IonItem>
              <IonItem>
                <IonIcon icon={checkmarkCircle} slot="start" />
                <IonLabel>
                  <h2>Mampang Prapatan</h2>
                  <p>
                  Jl. Mampang Prpt. Raya, RT.6/RW.6, Mampang Prpt., Kec. Mampang Prpt., Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12790
                  </p>
                </IonLabel>
              </IonItem>
              <IonItem>
                <IonIcon icon={checkmarkCircle} slot="start" />
                <IonLabel>
                  <h2>Kota Kasablanka</h2>
                  <p>
                  Jl. Raya Casablanca Raya No.Kav. 88, Menteng Dalam, Kec. Tebet, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12870
                  </p>
                </IonLabel>
              </IonItem>
              <IonItem>
                <IonIcon icon={checkmarkCircle} slot="start" />
                <IonLabel>
                  <h2>Blok M Square</h2>
                  <p>
                  Jl. Melawai 5, RT.3/RW.1, Melawai, Kec. Kby. Baru, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12160
                  </p>
                </IonLabel>
              </IonItem>
            </IonList>
          </IonContent>
        </IonModal>
      </IonContent>
    </IonPage>
  );
}

export default OrderWithNearby;
