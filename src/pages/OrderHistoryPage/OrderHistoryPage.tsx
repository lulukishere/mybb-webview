import {
  IonContent,
  IonRouterOutlet,
} from '@ionic/react';
import { Route, RouteComponentProps } from 'react-router-dom';
import HistoryListPage from '../HistoryListPage';
import HistoryDetailPage from '../HistoryDetailPage';

function OrderHistoryPage({ match }: RouteComponentProps) {
  return (
    <IonContent className="ion-padding">
      <IonRouterOutlet>
        <Route exact path={match.url} component={HistoryListPage} />
        <Route path={`${match.url}/detail/:id`} component={HistoryDetailPage} />
      </IonRouterOutlet>
    </IonContent>
  );
}

export default OrderHistoryPage;
