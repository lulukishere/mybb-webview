import  React from 'react';
import { IonRouterOutlet } from '@ionic/react';
import { Route, RouteComponentProps } from 'react-router-dom';
import OrderWithNearbyPage from '../OrderWithNearbyPage';

function OrderPage(props: RouteComponentProps) {
  const { match } = props;
  return (
    <IonRouterOutlet>
      <Route exact path={match.url} component={OrderWithNearbyPage} />
    </IonRouterOutlet>
  );
}

export default OrderPage;
