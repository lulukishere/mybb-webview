import React from 'react';
import {
  IonPage,
  IonHeader,
  IonToolbar,
  IonButtons,
  IonBackButton,
  IonTitle,
  IonContent,
} from '@ionic/react';
import useTabs from '../../hooks/useTabs';

function HistoryDetailPage() {
  const tabs = useTabs();

  React.useEffect(() => {
    tabs.setHide(true);

    return () => {
      tabs.setHide(false);
    };
  }, [tabs]);

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton />
          </IonButtons>
          <IonTitle>Detail Riwayat</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent className="ion-padding">
        History Detail Page
      </IonContent>
    </IonPage>
  );
}

export default HistoryDetailPage;