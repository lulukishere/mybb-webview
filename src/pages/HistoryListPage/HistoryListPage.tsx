import {
  IonContent,
  IonList,
  IonItem,
  IonLabel,
  IonIcon,
} from '@ionic/react';
import { car } from 'ionicons/icons';

function HistoryListPage() {
  return (
    <IonContent>
      <IonList>
        <IonItem routerLink="/history-detail/4">
          <IonIcon icon={car} slot="start" />
          <IonLabel>Ride ke Jl. Mampang Pra...</IonLabel>
        </IonItem>
        <IonItem routerLink="/history-detail/3">
          <IonIcon icon={car} slot="start" />
          <IonLabel>Ride ke Kota Kosablanka</IonLabel>
        </IonItem>
        <IonItem routerLink="/history-detail/2">
          <IonIcon icon={car} slot="start" />
          <IonLabel>Ride ke Blok M Square</IonLabel>
        </IonItem>
        <IonItem routerLink="/history-detail/1">
          <IonIcon icon={car} slot="start" />
          <IonLabel>Ride ke Bluebird Office Park</IonLabel>
        </IonItem>
      </IonList>
    </IonContent>
  );
}

export default HistoryListPage;
