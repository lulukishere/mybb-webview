import {
  IonHeader,
  IonToolbar,
  IonButtons,
  IonBackButton,
  IonTitle,
  IonContent,
  IonRouterOutlet,
  IonTabs,
  IonTabBar,
  IonTabButton,
  IonLabel,
} from '@ionic/react';
import { Redirect, Route, RouteComponentProps } from 'react-router-dom';
import OrderActivePage from '../OrderActivePage';
import OrderHistoryPage from '..//OrderHistoryPage';

function OrderPage({ match }: RouteComponentProps) {
  return (
    <>
      <IonHeader className="ion-no-border">
        <IonToolbar color="primary">
          <IonButtons slot="start">
            <IonBackButton />
          </IonButtons>
          <IonTitle>Order</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonTabs>
          <IonRouterOutlet>
            <Redirect exact path="/history" to="/history/done" />
            <Route exact path="/history/done" component={OrderHistoryPage} />
            <Route exact path="/history/active" component={OrderActivePage} />
          </IonRouterOutlet>
          <IonTabBar slot="top">
            <IonTabButton tab="done" href="/history/done">
              <IonLabel>Riwayat</IonLabel>
            </IonTabButton>
            <IonTabButton tab="active" href="/history/active">
              <IonLabel>Aktif</IonLabel>
            </IonTabButton>
          </IonTabBar>
        </IonTabs>
      </IonContent>
    </>
  );
}

export default OrderPage;
