import {
  IonPage,
  IonContent,
  IonSearchbar,
} from '@ionic/react';
import { locationOutline } from 'ionicons/icons';
import { useHistory } from 'react-router-dom';
import './styles.css';

function HomePage() {
  const history = useHistory();

  const handleClick = () => {
    history.push('/order');
  };

  return (
    <IonPage>
      <IonContent className="ion-padding">
        <IonSearchbar
          searchIcon={locationOutline}
          className="custom"
          placeholder="Mau kemana?"
          onClick={handleClick}
        />
      </IonContent>
    </IonPage>
  );
}

export default HomePage;
